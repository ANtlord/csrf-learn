import sys
from setuptools import find_packages, setup


def get_packages():
    kwargs = {
        'exclude': ['tests.*', 'tests'],
    }
    if 'flake8' in sys.argv:
        del kwargs['exclude']
    return find_packages(**kwargs)


setup(
    name='app',
    version='0.1.0',
    packages=get_packages(),
    url='',
    license='',
    author='',
    author_email='',
    description='',
    setup_requires=[],
    install_requires=[
        'Django==3.0.4',
        'uWSGI==2.0.18',
    ],
    include_package_data=True,
    entry_points={
        'console_scripts': ['buf=buf.__main__:main'],
    }
)
