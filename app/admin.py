from django.contrib import admin
from . import models
from django.db import router, transaction
from django.views.decorators import csrf

class TransactionAdmin(admin.ModelAdmin):
    @csrf.csrf_exempt
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        with transaction.atomic(using=router.db_for_write(self.model)):
            return self._changeform_view(request, object_id, form_url, extra_context)

    @csrf.csrf_exempt
    def add_view(self, request, form_url='', extra_context=None):
        return self.changeform_view(request, None, form_url, extra_context)

admin.site.register(models.Transaction, TransactionAdmin)
