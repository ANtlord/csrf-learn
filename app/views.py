from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView, CreateView
from django.http.request import HttpHeaders
from . import models

class Index(TemplateView):
    template_name = 'attacker.index.html'


class TransactionCreateView(LoginRequiredMixin, CreateView):
    model = models.Transaction
    fields = '__all__'
    success_url = '/'
    template_name = 'vulnerability.index.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['list'] = models.Transaction.objects.all()
        return data


class DumpHeaders:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.method == 'POST':
            with open('/tmp/app/post-headers.txt', 'a') as fd:
                fd.write(f'\n{request.path}\n')
                for k, v in request.headers.items():
                    fd.write(f'{k}: {v}\n')
        response = self.get_response(request)
        return response
