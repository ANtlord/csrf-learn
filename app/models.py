from django.db import models


class Transaction(models.Model):
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    card = models.IntegerField()
