#!/bin/bash

export DJANGO_SETTINGS_MODULE=app.settings
python && \
django-admin.py migrate --noinput && \
django-admin.py collectstatic --noinput && \
django-admin.py runserver 0.0.0.0:8000
# uwsgi --ini ./conf/uwsgi.ini
