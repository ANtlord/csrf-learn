FROM python:3.7.5
ENV PYTHONUNBUFFERED 1
RUN mkdir -p /code/app
WORKDIR /code
COPY setup.py /code/
COPY ./app/settings.py /code/app/
COPY ./app/__init__.py /code/app/
RUN pip install -e .
COPY . /code/
RUN python setup.py install
